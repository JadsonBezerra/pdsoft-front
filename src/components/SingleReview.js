import React, {Component} from 'react';
import axios from 'axios';
import NomeModelo from './Nome-modelo';

class SingleReview extends Component{

    constructor(props){
        super(props);
        this.state={
            item:props.item,
            CellPhoneName:[]
        }
    }


    render(){
        return(
            <li className="collection-item">
            <h5>
            <NomeModelo item={this.state.item.cellPhoneId}/>
            </h5>
            {this.state.item.rating} <i className="fa fa-star"></i> - {this.state.item.comments}
            </li>
        )
    }
}

export default SingleReview;