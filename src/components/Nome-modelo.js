import React, {Component} from 'react';
import axios from 'axios';

class NomeModelo extends Component{

    constructor(props){
        super(props);
        this.state={
            nome:"",
            item:props.item
        }
        
    }


    componentWillMount(){
        this.getName(this.state.item);
    }

    getName(id){
        axios.get('http://localhost:3000/api/CellPhones/'+id+
        '/modelo?access_token='+localStorage.getItem("access_token"))
        .then(response => {
            this.setState({nome:response.data.nome})
        }).catch(err=>{console.log(err); console.log(this.state)})
    }
    render(){
        return(
            <div>
                {this.state.nome}
            </div>
        )
    }
}

export default NomeModelo;