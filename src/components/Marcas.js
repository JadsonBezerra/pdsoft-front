import React, {Component} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';

class Marcas extends Component{
    constructor(){
        super();
        this.state = {
            Marcas: []
        }
    }


    componentWillMount(){
        this.getMarcas();
    }

    getMarcas(){
        axios.get('http://localhost:3000/api/marcas?access_token='
        +localStorage.getItem('access_token'))
        .then(response => {
            this.setState({Marcas: response.data},()=>{
                console.log(this.state);
            })
        }).catch(err => console.log(err));
    }

    render(){
        const MarcasItems = this.state.Marcas.map((Marca,i)=>{
            return(
                
                    <div className="collection-item">
                    <Link key={Marca.id} to={'/Marcas/'+Marca.id}>
                        <h6>
                        {Marca.nome}
                        </h6>
                        </Link>
                    </div>

            )
        })
        return(
            <div>
                <h4>Todos as Marcas</h4>
                <ul className="collection">
                    {MarcasItems}
                </ul>
            </div>
        )
    }
}

export default Marcas;