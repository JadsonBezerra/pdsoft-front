import React, {Component} from 'react';
import axios from 'axios';
import SingleCellPhone from './Single-Cell-Phone';


class CellPhones extends Component{
    constructor(){
        super();
        this.state = {
            CellPhones: []
        }
    }


    componentWillMount(){
        this.getCellPhones();
    }

    getCellPhones(){
        axios.get('http://localhost:3000/api/marcas/'+this.props.match.params.id
        +'/CellPhones?access_token='+localStorage.getItem('access_token'))
        .then(response => {
            this.setState({CellPhones: response.data},()=>{
                console.log(this.state);
            })
        }).catch(err => console.log(err));
    }

    render(){
        const CellPhonesItems = this.state.CellPhones.map((CellPhones,i)=>{
            return(
                    <SingleCellPhone  key={CellPhones.id} item={CellPhones}/>
            )
        })
        return(
            <div>
                <h4>Celulares da marca</h4>
                <ul className="collection">
                    {CellPhonesItems}
                </ul>
            </div>
        )
    }
}

export default CellPhones;