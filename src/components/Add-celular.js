import React, {Component} from 'react';
import axios from 'axios';
import Select from '@material-ui/core/Select' ;
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import NomeModelo from './Nome-modelo';

class AddReview extends Component{
    constructor(){
        super();
        this.state = {
            modeloId:0,
            SelectedCellPhone:0,
            Marcas:[]
        }
    }
    

    componentWillMount(){
        this.getCellPhones();
    }





    getCellPhones(){
        axios.get('http://localhost:3000/api/Marcas?access_token='
        +localStorage.getItem('access_token'))
        .then(response => {
            this.setState({Marcas: response.data},()=>{
                console.log(this.state);
            })
        }).catch(err => console.log(err));
    }


    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value }) 
        console.log(this.state);
    };


    onSubmit(e){
        
        axios.request({
            method:'post',
            url:'http://localhost:3000/api/modelos/',
            data:{nome:this.refs.modelo.value}
        }).then(response=>{
            this.setState({modeloId:response.data.id})
            console.log(this.state);
            const Celular={
                modeloId:this.state.modeloId
                ,ram:this.refs.ram.value
                ,capacidade:this.refs.capacidade.value
                ,processador:this.refs.processador.value
                ,sistema_operacional:this.refs.sistema.value
                ,camera:this.refs.camera.value
                ,bateria:this.refs.bateria.value
                ,marcaId:this.state.SelectedCellPhone
            }
    
            
            axios.request({
                method:'post',
                url:'http://localhost:3000/api/CellPhones?access_token='+localStorage.getItem('access_token'),
                data:Celular
            }).then(response=>{
                this.props.history.push('/');
            }).catch(err => console.log(err));
        }).catch(err => console.log(err));


        e.preventDefault();
    }

    render(){
        if(localStorage.getItem("ehadm")!=="true") this.props.history.push("/");
        const cellPhones = this.state.Marcas.map((cellPhone,i)=>{
            return(

                <MenuItem key={cellPhone.id} value={cellPhone.id}>
                {cellPhone.nome}
                </MenuItem>
            )
        })
        return(
            <div className="container">
                <h3>Adicionar Celular</h3>
                <br/>
                <form onSubmit={this.onSubmit.bind(this)}>
                    <h6>Marca</h6>
                    <div className="input-field">
                    <Select
                     fullWidth
                     value={this.state.SelectedCellPhone}
                     onChange={this.handleChange}
                     name="SelectedCellPhone"
                     >
                        {cellPhones}
                    </Select>
                    <div className="input-field">
                    <input type="text" name="modelo" ref ="modelo"/>
                    <label htmlFor="modelo">modelo</label>
                </div>
                    <div className="input-field">
                    <input type="text" name="ram" ref ="ram"/>
                    <label htmlFor="ram">ram</label>
                </div>
                <div className="input-field">
                    <input type="text" name="capacidade" ref ="capacidade"/>
                    <label htmlFor="capacidade">Armazenamento</label>
                </div>
                <div className="input-field">
                    <input type="text" name="processador" ref ="processador"/>
                    <label htmlFor="processador">processador</label>
                </div>
                <div className="input-field">
                    <input type="text" name="sistema" ref ="sistema"/>
                    <label htmlFor="sistema">sistema operacional</label>
                </div>
                <div className="input-field">
                    <input type="text" name="bateria" ref ="bateria"/>
                    <label htmlFor="bateria">bateria</label>
                </div>
                <div className="input-field">
                    <input type="text" name="camera" ref ="camera"/>
                    <label htmlFor="camera">camera</label>
                </div>
                    <input type="submit" value="Adicionar Celular" className="btn"/>
                    </div>
                </form>
            </div>
        )
    }
}

export default AddReview;