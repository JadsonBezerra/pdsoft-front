import React, {Component} from 'react';
import axios from 'axios';
import Select from '@material-ui/core/Select' ;
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import NomeModelo from './Nome-modelo';

class AddReview extends Component{
    constructor(){
        super();
        this.state = {
            CellPhones: [],
            CellNames: [],
            SelectedCellPhone:"",
            Rating:0,
            Comments:""
        }
    }
    

    componentWillMount(){
        this.getCellPhones();
    }





    getCellPhones(){
        axios.get('http://localhost:3000/api/CellPhones?access_token='
        +localStorage.getItem('access_token'))
        .then(response => {
            this.setState({CellPhones: response.data},()=>{
                console.log(this.state);
            })
        }).catch(err => console.log(err));
    }


    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value }) 
        console.log(this.state);
    };


    onSubmit(e){
        const Review={
            rating:this.state.Rating,
            comments:this.state.Comments,
            cellPhoneId:this.state.SelectedCellPhone,
            publisherId:localStorage.getItem('user_id')
        }
        
        axios.request({
            method:'post',
            url:'http://localhost:3000/api/Reviews?access_token='+localStorage.getItem('access_token'),
            data:Review
        }).then(response=>{
            this.props.history.push('/MyReviews');
        }).catch(err => console.log(err));
        e.preventDefault();
    }

    render(){
        const cellPhones = this.state.CellPhones.map((cellPhone,i)=>{
            return(

                <MenuItem key={cellPhone.id} value={cellPhone.id}>
                <NomeModelo item={cellPhone.id}/>
                </MenuItem>
            )
        })
        return(
            <div className="container">
                <h3>Adicionar Review</h3>
                <br/>
                <form onSubmit={this.onSubmit.bind(this)}>
                    <h6>Celular</h6>
                    <div className="input-field">
                    <Select
                     fullWidth
                     value={this.state.SelectedCellPhone}
                     onChange={this.handleChange}
                     name="SelectedCellPhone"
                     >
                        {cellPhones}
                    </Select>
                    </div>
                    <div className="input-field">
                    <Select
                     value={this.state.Rating}
                     onChange={this.handleChange}
                     name="Rating"
                     >
                        <MenuItem value={0}>
                        0 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={1}>
                        1 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={2}>
                        2 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={3}>
                        3 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={4}>
                        4 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={5}>
                        5 <i className="fa fa-star"/></MenuItem>
                    </Select>
                    </div>
                    <h6>Comentarios: </h6>
                    <div className="input-field">
                    <TextField
                    fullWidth
                    multiline={true}
                     onChange={this.handleChange}
                     value={this.state.Comments}
                      name="Comments" ref ="Comments"/>
                    </div>
                    <input type="submit" value="Enviar Review" className="btn"/>
                </form>
            </div>
        )
    }
}

export default AddReview;