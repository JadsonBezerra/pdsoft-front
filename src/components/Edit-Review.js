import React, {Component} from 'react';
import axios from 'axios';
import Select from '@material-ui/core/Select' ;
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import {Link} from 'react-router-dom';
import NomeModelo from './Nome-modelo';

class EditReview extends Component{
    constructor(props){
        super(props);
        this.state = {
            SelectedCellPhone:"",
            Rating:0,
            Comments:"",
            CellPhoneName:"",
            Date:""
        }
        console.log(this.state);
    }
    

    componentWillMount(){
        this.getReview();
    }


    getReview(){
        axios.get('http://localhost:3000/api/Reviews/'+this.props.match.params.id
        +'?access_token='+localStorage.getItem('access_token'))
        .then(response => {
            this.setState({SelectedCellPhone: response.data.cellPhoneId,
            Rating:response.data.rating,
            Comments:response.data.comments,
            Date:response.data.date},()=>{
                console.log(this.state);
            })
        }).catch(err => console.log(err));
        
    }


    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value }) 
        console.log(this.state);
    };


    onSubmit(e){
        const Review={
            rating:this.state.Rating,
            comments:this.state.Comments,
            cellPhoneId:this.state.SelectedCellPhone,
            publisherId:localStorage.getItem('user_id'),
            id:this.props.match.params.id,
            date:this.state.Date
        }
        console.log(Review);
        axios.request({
            method:'put',
            url:'http://localhost:3000/api/Reviews?access_token='+localStorage.getItem('access_token'),
            data:Review
        }).then(response=>{
            this.props.history.push('/MyReviews');
        }).catch(err => console.log(err));
        e.preventDefault();
    }

    render(){
        if(localStorage.getItem("user_id")==null) this.props.history.push("/SignIn");
        return(
            <div className="container">
                <h3>Editar Review</h3>
                <br/>
                
                <form onSubmit={this.onSubmit.bind(this)}>
                    <div className="input-field">
                    <Select
                     value={this.state.Rating}
                     onChange={this.handleChange}
                     name="Rating"
                     >
                        <MenuItem value={0}>
                        0 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={1}>
                        1 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={2}>
                        2 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={3}>
                        3 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={4}>
                        4 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={5}>
                        5 <i className="fa fa-star"/></MenuItem>
                    </Select>
                    </div>
                    <h6>Comments: </h6>
                    <div className="input-field">
                    <TextField
                    fullWidth
                    multiline={true}
                     onChange={this.handleChange}
                     value={this.state.Comments}
                      name="Comments" ref ="Comments"/>
                    </div>
                    <input type="submit" value="Confirmar" className="btn"/>
                    <Link to={`/DeleteReview/${this.props.match.params.id}`}>
                        <button className="btn red right">Deletar</button>
                    </Link>
                </form>
            </div>
        )
    }
}

export default EditReview;