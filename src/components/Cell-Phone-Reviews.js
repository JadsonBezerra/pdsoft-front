import React, {Component} from 'react';
import axios from 'axios';
import SingleReview from './SingleReview';      
import NomeModelo from './Nome-modelo';

class CellPhoneReviews extends Component{
    constructor(props){
        super(props);
        this.state = {
            reviews: [],
            CellPhone:""
        }
    }


    componentWillMount(){
        this.getCellPhoneName();
        this.getReviews();
    }

    getReviews(){
        axios.get('http://localhost:3000/api/CellPhones/'+this.props.match.params.id
        +'/reviews?access_token='+localStorage.getItem('access_token'))
        .then(response => {
            this.setState({reviews: response.data},()=>{
                console.log(this.state);
            })
        }).catch(err => console.log(err));
    }

    getCellPhoneName(){
        axios.get('http://localhost:3000/api/CellPhones/'+this.props.match.params.id
        +'?access_token='+localStorage.getItem('access_token'))
        .then(response => {
            this.setState({CellPhone:response.data})
            console.log(this.state)
        }).catch(err => console.log(err));
    }

    render(){
        const reviewsItems = this.state.reviews.map((review,i)=>{
            return(
                <SingleReview key={review.id} item={review}/>
            )
        })
        return(
            <div>
                <h4>Reviews de <NomeModelo item={this.props.match.params.id}/></h4>
                <h6>Ram: {this.state.CellPhone.ram}| 
                  Armazenamento: {this.state.CellPhone.capacidade}</h6>
                <h6>Camera:{this.state.CellPhone.camera}| 
                 Sistema operacional:{this.state.CellPhone.sistema_operacional}</h6>
                <ul className="collection">
                    {reviewsItems}
                </ul>
            </div>
        )
    }
}

export default CellPhoneReviews;