import React, {Component} from 'react';
import axios from 'axios';
import SingleReview from './SingleReview';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import { Link } from 'react-router-dom';


class My_Reviews extends Component{
    constructor(){
        super();
        this.state = {
            reviews: []
        }
    }


    componentWillMount(){
        this.getReviews();
    }

    getReviews(){
        axios.get('http://localhost:3000/api/Reviewers/'+localStorage.getItem('user_id')
        +'/reviews?access_token='+localStorage.getItem('access_token'))
        .then(response => {
            this.setState({reviews: response.data},()=>{
                console.log(this.state);
            })
        }).catch(err => console.log(err));
    }

    render(){
        if(localStorage.getItem("user_id")==null) this.props.history.push("/SignIn");
        const reviewsItems = this.state.reviews.map((review,i)=>{
            return(
                <div  key={review.id}  className="collection-item">
                <SingleReview item={review}/>
                <Link to={`/DeleteReview/${review.id}`} >
                <Fab className="right"  size="small" color="secondary" aria-label="Edit">
                    <Icon>delete_icon</Icon>
                </Fab>
                </Link>
                <Link to={`/EditReview/${review.id}`} >
                <Fab  className="right" size="small" color="primary" aria-label="Edit">
                    <Icon>edit_icon</Icon>
                </Fab>
                </Link>
                <br/>
                <br/>
                </div>
            )
        })
        return(
            <div>
                <h4>My Reviews</h4>
                <ul className="collection">
                    {reviewsItems}
                </ul>
            </div>
        )
    }
}

export default My_Reviews;