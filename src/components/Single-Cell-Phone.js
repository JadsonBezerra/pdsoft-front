import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import NomeModelo from './Nome-modelo';

class SingleCellPhones extends Component{

    constructor(props){
        super(props);
        this.state={
            item:props.item
        }
    }  



    render(){
        return(
            
            <li className="collection-item">
            <Link to={`/CellPhoneReviews/${this.state.item.id}`} style={{color:"#000"}}>
            <h5>
            <NomeModelo item={this.state.item.id}/>   <i className="fa fa-phone"></i>
            </h5>
            </Link>
            </li>
        )
    }
}

export default SingleCellPhones;