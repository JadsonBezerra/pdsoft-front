import React, {Component} from 'react';
import axios from 'axios';


class AddMarca extends Component{


    onSubmit(e){
        axios.request({
            method:'post',
            url:'http://localhost:3000/api/marcas/',
            data:{nome:this.refs.marca.value}
        }).then(response=>{
            this.props.history.push('/AddCelular');
        }).catch(err => console.log(err));
        e.preventDefault();
        
    }

    render(){
        if(localStorage.getItem("ehadm")!=="true") this.props.history.push("/");
        return(
            <div>
                <div className="wrapper">
            <div className="container">
            <h4>Adicionar Marca</h4>
                <form onSubmit={this.onSubmit.bind(this)}>
                <div className="input-field">
                    <input type="text" name="marca" ref ="marca"/>
                    <label htmlFor="marca">Nome da marca</label>
                </div>
                </form>
             </div>
             </div>
            </div>
        )
    }
}

export default AddMarca;